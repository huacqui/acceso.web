package py.edu.ucsa.acceso.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = AppInitializer.class)
class ApplicationConfig {


}