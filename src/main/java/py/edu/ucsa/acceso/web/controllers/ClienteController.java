package py.edu.ucsa.acceso.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import py.edu.ucsa.acceso.web.dto.ClienteDTO;
import py.edu.ucsa.acceso.web.services.ClienteService;

@Controller
@Secured("ROLE_USER")
@RequestMapping("/clientes")
@SessionAttributes({ "listaClientes" })
public class ClienteController {
	@Autowired
	private ClienteService clienteService;
	
	@ModelAttribute("module")
    public String module() {
        return "clientes";
    }

	@RequestMapping(value = "/grilla", method = RequestMethod.GET)
	public String listarTodos(Model model) {
		List<ClienteDTO> clientes = clienteService.listar();
		model.addAttribute("listaClientes", clientes);
		return "layout/lista-clientes"; // clientes.html
	}
	
	@RequestMapping(value = "clientes/{id}", method = RequestMethod.GET)
    public String task(@PathVariable("id") Long id, Model model) {
        model.addAttribute("cliente", clienteService.getById(id));
        return "layout/form-clientes";
    }
}
